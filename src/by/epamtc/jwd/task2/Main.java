package by.epamtc.jwd.task2;

import by.epamtc.jwd.task2.exception.WrongMonthException;
import by.epamtc.jwd.util.Communicator;

public class Main {

    public static void main(String[] args) {
        Communicator in = new Communicator();

        int month = in.nextInt();
        int year = in.nextInt();

        try {
            Calendar calendar = new Calendar();
            System.out.println(calendar.countDaysCount(month, year));
        }catch (WrongMonthException e) {
            //log
            System.out.println("Month must be in range of 1..12");
        }
    }
}
