package by.epamtc.jwd.task2.exception;

public class WrongMonthException extends Exception{
    public WrongMonthException() {
        super();
    }

    public WrongMonthException(String message) {
        super(message);
    }

    public WrongMonthException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongMonthException(Throwable cause) {
        super(cause);
    }
}
