package by.epamtc.jwd.task2;

import by.epamtc.jwd.task2.exception.WrongMonthException;

public class Calendar {
    int countDaysCount(int _month, int year) throws WrongMonthException {
        if (_month < 1 || _month > 12)
            throw new WrongMonthException("Wrong month number");

        int count = 30;
        Month month = Month.getMonth(_month-1);
        switch (month){
            case JANUARY:
            case MARCH:
            case MAY:
            case JULY:
            case AUGUST:
            case OCTOBER:
            case DECEMBER:
                count = 31;
                break;

            case FEBRUARY:
                count = (year % 4 == 0)?29:28;
                break;
        }
        return count;
    }
}
