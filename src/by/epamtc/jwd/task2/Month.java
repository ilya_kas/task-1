package by.epamtc.jwd.task2;

public enum Month {
    JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER;

    static Month getMonth(int nom){
        return values()[nom];
    }
}
