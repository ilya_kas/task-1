package by.epamtc.jwd.task7.utils;

public class Comparator {
    public Dot compare(Dot dot1, Dot dot2){
        if (dot1 == null) return dot2;
        if (dot2 == null) return dot1;

        double dist1 = dot1.getDist();
        double dist2 = dot2.getDist();

        if (dist1 < dist2)
            return dot1;
        else if (dist1 > dist2)
            return dot2;
        else
            return null;
    }
}
