package by.epamtc.jwd.task7.utils;

public class Dot {
    int x, y;

    public Dot(int _x, int _y){
        x = _x;
        y = _y;
    }

    //расстояние до (0,0)
    public double getDist(){
        return Math.sqrt(x*x + y*y);
    }

    @Override
    public String toString() {
        String ans = "(" + x + "," + y + ") dot";
        return ans;
    }
}
