package by.epamtc.jwd.task7;

import by.epamtc.jwd.task7.utils.Comparator;
import by.epamtc.jwd.task7.utils.Dot;
import by.epamtc.jwd.util.Communicator;

public class Main {
    public static void main(String[] args) {
        Communicator in = new Communicator();

        int x = in.nextInt();
        int y = in.nextInt();
        Dot first = new Dot(x,y);

        x = in.nextInt();
        y = in.nextInt();
        Dot second = new Dot(x,y);

        Comparator comparator = new Comparator();
        Dot nearest = comparator.compare(first, second);
        if (nearest != null)
            System.out.println(nearest + " is the nearest");
        else
            System.out.println("Distances are equal");
    }
}
