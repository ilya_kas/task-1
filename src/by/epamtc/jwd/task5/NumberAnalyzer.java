package by.epamtc.jwd.task5;

public class NumberAnalyzer {
    boolean isComplete(int num){
        int sum = 0; //сумма делителей
        for (int i=2; i<Math.sqrt(num); i++)
            if (num % i == 0)
                sum += i + num/i;
        sum++; //единица как делитель
        return sum==num;
    }
}
