package by.epamtc.jwd.task5;

import by.epamtc.jwd.util.Communicator;

public class Main {

    public static void main(String[] args) {
        Communicator in = new Communicator();

        int x = in.nextInt();

        NumberAnalyzer analyzer = new NumberAnalyzer();
        System.out.println(analyzer.isComplete(x));
    }
}
