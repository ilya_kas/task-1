package by.epamtc.jwd.task1;

import by.epamtc.jwd.util.Communicator;

public class Main {

    public static void main(String[] args) {
        Communicator in = new Communicator();

        int num = in.nextInt();

        Logic logic = new Logic();
        System.out.println(logic.calcLastDigit(num));
    }
}
