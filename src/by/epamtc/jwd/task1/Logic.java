package by.epamtc.jwd.task1;

public class Logic {
    int calcLastDigit(int number){
        int lastDigit = number % 10;
        int ans = (lastDigit * lastDigit) % 10;
        return ans;
    }
}
