package by.epamtc.jwd.task8;

public class FunctionResolver {

    double count(long x){
        double f;
        if (x < 3)
            f = 1f / (x*x*x - 6);
        else
            f = (-1) * x * x + 3*x + 9;
        return f;
    }
}
