package by.epamtc.jwd.task8;

import by.epamtc.jwd.util.Communicator;

public class Main {

    public static void main(String[] args) {
        Communicator in = new Communicator();

        int x = in.nextInt();

        FunctionResolver functionResolver = new FunctionResolver();
        System.out.println(functionResolver.count(x));
    }
}
