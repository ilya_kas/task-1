package by.epamtc.jwd.task9;

import by.epamtc.jwd.task9.exception.NegativeRadiusException;
import by.epamtc.jwd.util.Communicator;

public class Main {
    public static void main(String[] args) {
        Communicator in = new Communicator();

        int r = in.nextInt();

        try {
            Circle circle = new Circle(r);
            System.out.println(circle.getLength());
            System.out.println(circle.getSquare());
        } catch (NegativeRadiusException e) {
            //log
            System.out.println("Radius must be non-negative");
        }
    }
}
