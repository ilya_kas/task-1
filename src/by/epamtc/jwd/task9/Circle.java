package by.epamtc.jwd.task9;

import by.epamtc.jwd.task9.exception.NegativeRadiusException;

public class Circle {
    private int r;

    Circle(int _r) throws NegativeRadiusException {
        if (_r < 0)
            throw new NegativeRadiusException();
        r = _r;
    }

    double getLength(){
        return 2 * Math.PI * r;
    }

    double getSquare(){
        return Math.PI * r * r;
    }
}
