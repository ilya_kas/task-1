package by.epamtc.jwd.util;

import java.util.Scanner;

public class Communicator {

    Scanner scan = new Scanner(System.in);

    public int nextInt(){
        while (!scan.hasNextInt()) {
            scan.next();
            System.out.println("Input an integer");
        }
        int num = scan.nextInt();
        return num;
    }

    public double nextDouble(){
        while (!scan.hasNextDouble()) {
            scan.next();
            System.out.println("Input a double");
        }
        double num = scan.nextDouble();
        return num;
    }

    public long nextLong(){
        while (!scan.hasNextLong()) {
            scan.next();
            System.out.println("Input a long");
        }
        long num = scan.nextLong();
        return num;
    }
}
