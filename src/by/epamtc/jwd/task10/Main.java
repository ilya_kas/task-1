package by.epamtc.jwd.task10;

import by.epamtc.jwd.util.Communicator;

public class Main {

    public static void main(String[] args) {
        Communicator in = new Communicator();

        double a = in.nextDouble();
        double b = in.nextDouble();

        if (a > b) {
            //log
            System.out.println("A should be bigger then B");
            return;
        }

        double h = in.nextDouble();
        if (h < 0){
            //log
            System.out.println("H should be positive");
            return;
        }

        FunctionResolver functionResolver = new FunctionResolver();
        System.out.println("  x:           F(x):");
        for (double x = a; x <= b; x += h)
            System.out.printf("%10f   %10f%n", x, functionResolver.func(x));
    }
}
