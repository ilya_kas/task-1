package by.epamtc.jwd.task4;

public class Logic {

     private int countEven(int[] numbers){
        int res = 0;

        for (int i=0; i<numbers.length; i++)
            if (numbers[i] % 2 == 0)
                res++;
        return res;
    }

    boolean isEnough(int[] input){
         int evenCount = countEven(input);
         boolean ans = evenCount>=2;
         return ans;
    }
}
