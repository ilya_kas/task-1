package by.epamtc.jwd.task4;

import by.epamtc.jwd.util.Communicator;

public class Main {

    public static void main(String[] args) {
        Communicator in = new Communicator();

        int[] input = new int[4];
        for (int i=0; i<4; i++)
            input[i] = in.nextInt();

        Logic logic = new Logic();
        System.out.println(logic.isEnough(input));
    }
}
