package by.epamtc.jwd.task3;

import by.epamtc.jwd.task3.exception.WrongSquareException;
import by.epamtc.jwd.util.Communicator;

public class Main {

    public static void main(String[] args) {
        Communicator in = new Communicator();

        double square = in.nextDouble();

        try {
            SuperSquare figure = new SuperSquare(square);
            System.out.println(figure.getSmallSquare());
        }catch (WrongSquareException e){
            //log exception
            System.out.println("Square must be positive");
        }
    }
}
