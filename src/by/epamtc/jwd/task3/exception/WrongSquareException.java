package by.epamtc.jwd.task3.exception;

public class WrongSquareException extends Exception{
    public WrongSquareException() {
        super();
    }

    public WrongSquareException(String message) {
        super(message);
    }

    public WrongSquareException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongSquareException(Throwable cause) {
        super(cause);
    }
}
