package by.epamtc.jwd.task3;

import by.epamtc.jwd.task3.exception.WrongSquareException;

public class SuperSquare {
    private double square;

    SuperSquare(double square) throws WrongSquareException {
        if (square <= 0)
            throw new WrongSquareException();
        this.square = square;
    }

    double getSmallSquare(){
        double ans = Math.sqrt(square/2);
        return ans;
    }
}
