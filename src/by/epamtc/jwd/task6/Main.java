package by.epamtc.jwd.task6;

import by.epamtc.jwd.task6.exception.NegativeTimeException;
import by.epamtc.jwd.util.Communicator;

public class Main {

    public static void main(String[] args) {
        Communicator in = new Communicator();

        long x = in.nextLong();

        try {
            Time time = new Time(x);
            System.out.printf("Hours: %d\n",time.getHours());
            System.out.printf("Minutes: %d\n",time.getMinutes());
            System.out.printf("Seconds: %d\n",time.getSeconds());
        }catch (NegativeTimeException e){
            //log
            System.out.println("Time must be non-negative");
        }
    }
}
