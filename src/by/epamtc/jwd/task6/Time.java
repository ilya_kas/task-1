package by.epamtc.jwd.task6;

import by.epamtc.jwd.task6.exception.NegativeTimeException;

public class Time {
    private final long SECONDS_IN_HOUR = 3600;
    private final long SECONDS_IN_MINUTE = 60;

    private long hours;
    private long minutes;
    private long seconds;

    Time(long seconds) throws NegativeTimeException {
        if (seconds < 0)
            throw new NegativeTimeException();
        hours = seconds / SECONDS_IN_HOUR;
        minutes = seconds / SECONDS_IN_MINUTE;
        this.seconds = seconds;
    }

    public long getHours() {
        return hours;
    }

    public long getMinutes() {
        return minutes;
    }

    public long getSeconds() {
        return seconds;
    }
}
